var NO_VALID_PLATE = 'NO VALID PLATE WITH THOSE CHARACTERS';
var VALID_PLATE = 'VALID PLATE';

function isValidPlate(plate) { // eslint-disable-line

  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  var type='';

  if(plate.match(re) !== null) {
    type=VALID_PLATE;
  } else {
    type= NO_VALID_PLATE;
  }

  gtag('event','click',{ // eslint-disable-line
    'event_category': 'validate',
    'event_label': type
  });
  return type;
}