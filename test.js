test("Valid plate",function(assert){

	assert.equal(isValidPlate('4892CPR'),VALID_PLATE,"VALID PLATE");
	assert.equal(isValidPlate('1234SNZ'),VALID_PLATE,"VALID PLATE");
	assert.equal(isValidPlate('4298YTW'),VALID_PLATE,"VALID PLATE");
	assert.equal(isValidPlate('9611TKM'),VALID_PLATE,"VALID PLATE");
	assert.equal(isValidPlate('6751LVS'),VALID_PLATE,"VALID PLATE");

});

test("No valid plate",function(assert){

	assert.equal(isValidPlate('4892CPA'),NO_VALID_PLATE,"USE OF VOWELS ON THE LETTER PART");
	assert.equal(isValidPlate('2192EPS'),NO_VALID_PLATE,"USE OF VOWELS ON THE LETTER PART");
	assert.equal(isValidPlate('9876COA'),NO_VALID_PLATE,"USE OF VOWELS ON THE LETTER PART");
	assert.equal(isValidPlate('1111CPU'),NO_VALID_PLATE,"USE OF VOWELS ON THE LETTER PART");
	assert.equal(isValidPlate('3256SIW'),NO_VALID_PLATE,"USE OF VOWELS ON THE LETTER PART");

	assert.equal(isValidPlate('2233SWÑ'),NO_VALID_PLATE,"USE OF THE INVALID LETTER Ñ ");
	assert.equal(isValidPlate('7523TQT'),NO_VALID_PLATE,"USE OF THE INVALID LETTER Q ");

	assert.equal(isValidPlate('22333WL'),NO_VALID_PLATE,"USE OF MORE DIGITS THAN THE NECESSARY ");
	assert.equal(isValidPlate('223TSSR'),NO_VALID_PLATE,"USE OF MORE LETTERS THAN THE NECCESSARY ");

	assert.equal(isValidPlate('2233SW'),NO_VALID_PLATE,"USE OF LESS DIGITS THAN THE NECCESARY");

	assert.equal(isValidPlate('2675trs'),VALID_PLATE,"USE OF LOWERCASE LETTERS");
	assert.equal(isValidPlate('2675TRs'),VALID_PLATE,"USE OF LOWERCASE LETTERS");

	assert.equal(isValidPlate('VBP3456'),NO_VALID_PLATE,"WRONG ORDER OF THE CHARACTERS");
	assert.equal(isValidPlate('V34DD56'),NO_VALID_PLATE,"WRONG ORDER OF THE CHARACTERS");

	assert.equal(isValidPlate('123-VBT'),NO_VALID_PLATE,"USE OF ADITTIONAL CHARACTERS");
	assert.equal(isValidPlate('456?RRR'),NO_VALID_PLATE,"USE OF ADITTIONAL CHARACTERS");

});